<?php

namespace App\Http\Controllers;

use App\Models\nilai;
use App\Models\mahasiswa;
use App\Models\matkul;
use Illuminate\Http\Request;

class NilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $matkul = matkul::all()->first();
        $nilai = nilai::all();
        return view('nilai.index', compact(
            'matkul','nilai'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mahasiswa = mahasiswa::all();
        $matkul = matkul::all();
        return view('nilai.create', compact(
            'mahasiswa','matkul'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            
            'mahasiswa_id' => 'required',
            'na_matkul1' => 'required',
            'na_matkul2' => 'required',
            'na_matkul3' => 'required',
            'na_matkul4' => 'required',
            'na_matkul5' => 'required',
            'na_matkul6' => 'required',
            'na_matkul7' => 'required',
            'na_matkul8' => 'required',
            'na_matkul9' => 'required',
            'na_matkul10' => 'required',
            'na_matkul11' => 'required',
            'na_matkul12' => 'required',
            'na_matkul13' => 'required',
            'na_matkul14' => 'required',
            'na_matkul15' => 'required',
            'na_matkul16' => 'required',
            'na_matkul17' => 'required',
            'na_matkul18' => 'required',
            'na_matkul19' => 'required',
            'na_matkul20' => 'required',
            'na_matkul21' => 'required',
            'na_matkul22' => 'required',
            'na_matkul23' => 'required',
            'na_matkul24' => 'required'
        ],
        [
            'mahasiswa_id.required' => 'Mahasiswa tidak boleh kosong',
            'na_matkul1.required' => 'Nilai tidak boleh kosong',
            'na_matkul2.required' => 'Nilai tidak boleh kosong',
            'na_matkul3.required' => 'Nilai tidak boleh kosong',
            'na_matkul4.required' => 'Nilai tidak boleh kosong',
            'na_matkul5.required' => 'Nilai tidak boleh kosong',
            'na_matkul6.required' => 'Nilai tidak boleh kosong',
            'na_matkul7.required' => 'Nilai tidak boleh kosong',
            'na_matkul8.required' => 'Nilai tidak boleh kosong',
            'na_matkul9.required' => 'Nilai tidak boleh kosong',
            'na_matkul10.required' => 'Nilai tidak boleh kosong',
            'na_matkul11.required' => 'Nilai tidak boleh kosong',
            'na_matkul12.required' => 'Nilai tidak boleh kosong',
            'na_matkul13.required' => 'Nilai tidak boleh kosong',
            'na_matkul14.required' => 'Nilai tidak boleh kosong',
            'na_matkul15.required' => 'Nilai tidak boleh kosong',
            'na_matkul16.required' => 'Nilai tidak boleh kosong',
            'na_matkul17.required' => 'Nilai tidak boleh kosong',
            'na_matkul18.required' => 'Nilai tidak boleh kosong',
            'na_matkul19.required' => 'Nilai tidak boleh kosong',
            'na_matkul20.required' => 'Nilai tidak boleh kosong',
            'na_matkul21.required' => 'Nilai tidak boleh kosong',
            'na_matkul22.required' => 'Nilai tidak boleh kosong',
            'na_matkul23.required' => 'Nilai tidak boleh kosong',
            'na_matkul24.required' => 'Nilai tidak boleh kosong'
        ]);

        // $matkul = matkul::where('id', $request['matkul_id'])->first();
        
        nilai::create([
            "mahasiswa_id" => $request['mahasiswa_id'],
            "na_matkul1" => $request['na_matkul1'],
            "na_matkul2" => $request['na_matkul2'],
            "na_matkul3" => $request['na_matkul3'],
            "na_matkul4" => $request['na_matkul4'],
            "na_matkul5" => $request['na_matkul5'],
            "na_matkul6" => $request['na_matkul6'],
            "na_matkul7" => $request['na_matkul7'],
            "na_matkul8" => $request['na_matkul8'],
            "na_matkul9" => $request['na_matkul9'],
            "na_matkul10" => $request['na_matkul10'],
            "na_matkul11" => $request['na_matkul11'],
            "na_matkul12" => $request['na_matkul12'],
            "na_matkul13" => $request['na_matkul13'],
            "na_matkul14" => $request['na_matkul14'],
            "na_matkul15" => $request['na_matkul15'],
            "na_matkul16" => $request['na_matkul16'],
            "na_matkul17" => $request['na_matkul17'],
            "na_matkul18" => $request['na_matkul18'],
            "na_matkul19" => $request['na_matkul19'],
            "na_matkul20" => $request['na_matkul20'],
            "na_matkul21" => $request['na_matkul21'],
            "na_matkul22" => $request['na_matkul22'],
            "na_matkul23" => $request['na_matkul23'],
            "na_matkul24" => $request['na_matkul24'],
        ]);
        return redirect('nilai');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\nilai  $nilai
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $matkul = matkul::all();
        $d_nilai = nilai::all();
        $nilai = nilai::where('id', $id)->first();
        return view('nilai.show', compact('nilai','matkul','d_nilai'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\nilai  $nilai
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mahasiswa = mahasiswa::all();
        $matkul = matkul::all();
        $nilai = nilai::where('id', $id)->first();
        return view('nilai.edit', compact('matkul','mahasiswa','nilai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\nilai  $nilai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            
            'mahasiswa_id' => 'required',
            'matkul_id' => 'required',
            'nilai' => 'required',
        ],
        [
            
            'mahasiswa_id.required' => 'Mahasiswa tidak boleh kosong',
            'matkul_id.required' => 'Mata Kuliah tidak boleh kosong',
            'nilai.required' => 'Nilai tidak boleh kosong',
        ]);

        

        $tanya = nilai::where('id', $id)->update([
            "mahasiswa_id" => $request['mahasiswa_id'],
            "matkul_id" => $request['matkul_id'],
            "nilai" => $request['nilai'],
            "nilaiakhir" => $request['nilai'] * $matkul->sks
        ]);
        return redirect('nilai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\nilai  $nilai
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = nilai::where('id', $id)->delete();
        return redirect('nilai');
    }
}
