<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\matkul;
use App\Models\nilai;

class analisisController extends Controller
{
    public function index()
    {
        $matkul = matkul::all();
        $nilai = nilai::all();
        return view('analisis.index', compact('matkul','nilai'));
    }
}
