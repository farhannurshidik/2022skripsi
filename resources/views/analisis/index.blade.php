@extends('partial.master')
@push('style')
    <link rel="stylesheet" href="{{ asset('layout/assets/css/lib/datatable/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">ANALISIS <i>K-MEANS CLUSTERING</i></h4>
                                
                                <a href="{{ route('mahasiswa.create') }}" class="btn btn-primary btn-round ml-auto">
                                    Hitung Analisis
                                </a>
                                
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="#">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Aksi</th>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($nilai as $key=>$data)
                                        <tr>
                                            <td>0</td>
                                            <td>{{ $data->mahasiswa->nim }}</td>
                                            <td>{{ $data->mahasiswa->nama }}</td>
                                        </tr>
                                    @endforeach             
                                </tbody>
                            </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('layout/assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/init/datatables-init.js') }}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#bootstrap-data-table-export').DataTable();
    } );
    </script>
@endpush