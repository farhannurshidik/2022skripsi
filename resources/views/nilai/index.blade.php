@extends('partial.master')
@push('style')
    <link rel="stylesheet" href="{{ asset('layout/assets/css/lib/datatable/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Data Nilai Mata Kuliah</h4>
                                
                                <a href="{{ route('nilai.create') }}" class="btn btn-primary btn-round ml-auto">
                                    Tambah Data  
                                </a>
                                
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width=20%>NIM</th>
                                        <th width=40%>Nama Mahasiswa</th>
                                        <th width=20%>Nilai</th>
                                        <th width=20%>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach ($nilai as $key=>$data)
                                        <tr>
                                            <td>{{ $data->mahasiswa->nim }}</td>
                                            <td>{{ $data->mahasiswa->nama }}</td>
                                            <td>Tersimpan</td>
                                            <td>
                                            <form action="{{route('nilai.destroy', $data->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                {{-- <a href="{{route('nilai.show', $data->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-info" aria-hidden="true"></i></a> --}}
                                                <a href="{{route('nilai.edit', $data->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <button type="submit" class="btn btn-danger my-1 btn-sm fa fa-trash">
                                            </form>
                                            </td>
                                        </tr>
                                    @endforeach              
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('layout/assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/jszip.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/vfs_fonts.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/buttons.print.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('layout/assets/js/init/datatables-init.js') }}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#bootstrap-data-table-export').DataTable();
    } );
    </script>
@endpush