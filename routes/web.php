<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\MatkulController;
use App\Http\Controllers\NilaiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [App\Http\Controllers\mhscekController::class, 'index'])->name('mhscek');

// Auth::routes();
Route::prefix('wp-admin')->group(function () {
    Auth::routes();
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('mahasiswa', MahasiswaController::class);
Route::resource('matkul', MatkulController::class);
Route::resource('nilai', NilaiController::class);
Route::get('/profile/index', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile.index');
Route::get('/cek', [App\Http\Controllers\mhscekController::class, 'show'])->name('mhscek.show');
Route::get('/analisis', [App\Http\Controllers\analisisController::class, 'index'])->name('analisis.index');